# php-extended/php-io-object

Library that implements the library php-extended/php-io-interface.

![coverage](https://gitlab.com/php-extended/php-io-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-io-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-io-object ^8`


## Basic Usage

For the output stream:

```php 

use PhpExtended\Io\ConsoleOutputStream;

$console = new ConsoleOutputStream();
$console->write('<your string here>', true);	// equivalent to echo '<your string>'."\n";
```

For the input stream:

```php

use PhpExtended\Io\ConsoleInputStream;

$console = new ConsoleInputStream();
$input = $console->readline(); 				// equivalent to readline();

```


## License

MIT (See [license file](LICENSE)).
