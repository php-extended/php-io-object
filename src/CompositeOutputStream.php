<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-io-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Io;

use Iterator;

/**
 * CompositeOutputStream class file.
 *
 * This class represents an branching in the output route. This node will
 * duplicate the output for each of its children.
 *
 * @author Anastaszor
 */
class CompositeOutputStream implements OutputStreamInterface
{
	
	/**
	 * All the children output streams of this node.
	 *
	 * @var array<string, OutputStreamInterface>
	 */
	protected array $_routes = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a route to this node with a specific name, or id.
	 *
	 * @param string $ident the name of the stream
	 * @param OutputStreamInterface $stream the children stream
	 */
	public function setRoute(string $ident, OutputStreamInterface $stream) : void
	{
		$this->_routes[$ident] = $stream;
	}
	
	/**
	 * Removes the route for this node with given name or id.
	 *
	 * @param string $ident
	 */
	public function removeRoute(string $ident) : void
	{
		unset($this->_routes[$ident]);
	}
	
	/**
	 * Clears all the routes for this node.
	 */
	public function removeAllRoutes() : void
	{
		$this->_routes = [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::write()
	 */
	public function write(string $bytes) : int
	{
		$written = 0;
		
		foreach($this->_routes as $stream)
		{
			$written = \max($written, $stream->write($bytes));
		}
		
		return $written;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeLn()
	 */
	public function writeLn(string $bytes) : int
	{
		$written = 0;
		
		foreach($this->_routes as $stream)
		{
			$written = \max($written, $stream->writeLn($bytes));
		}
		
		return $written;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeArray()
	 */
	public function writeArray(array $bytes) : int
	{
		$len = 0;
		
		foreach($bytes as $string)
		{
			$written = 0;
			
			foreach($this->_routes as $stream)
			{
				$written = \max($written, $stream->write($string));
			}
			
			$len += $written;
		}
		
		return $len;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeArrayLn()
	 */
	public function writeArrayLn(array $bytes) : int
	{
		$len = 0;
		
		foreach($bytes as $string)
		{
			$written = 0;
			
			foreach($this->_routes as $stream)
			{
				$written = \max($written, $stream->writeLn($string));
			}
			
			$len += $written;
		}
		
		return $len;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeIterator()
	 */
	public function writeIterator(Iterator $iterator) : int
	{
		$len = 0;
		
		foreach($iterator as $string)
		{
			$written = 0;
			
			foreach($this->_routes as $stream)
			{
				$written = \max($written, $stream->write($string));
			}
			
			$len += $written;
		}
		
		return $len;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeIteratorLn()
	 */
	public function writeIteratorLn(Iterator $iterator) : int
	{
		$len = 0;
		
		foreach($iterator as $string)
		{
			$written = 0;
			
			foreach($this->_routes as $stream)
			{
				$written = \max($written, $stream->writeLn($string));
			}
			
			$len += $written;
		}
		
		return $len;
	}
	
}
