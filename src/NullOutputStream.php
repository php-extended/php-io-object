<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-io-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Io;

use Iterator;

/**
 * NullOutputStream class file.
 *
 * This class is provided as a placeholder when an outputstrem is needed.
 * This class outputs no data, but behaves like it had.
 *
 * @author Anastaszor
 */
class NullOutputStream implements OutputStreamInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::write()
	 */
	public function write(string $bytes) : int
	{
		return 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeLn()
	 */
	public function writeLn(string $bytes) : int
	{
		return 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeArray()
	 */
	public function writeArray(array $bytes) : int
	{
		return 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeArrayLn()
	 */
	public function writeArrayLn(array $bytes) : int
	{
		return 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeIterator()
	 */
	public function writeIterator(Iterator $iterator) : int
	{
		return 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeIteratorLn()
	 */
	public function writeIteratorLn(Iterator $iterator) : int
	{
		return 0;
	}
	
}
