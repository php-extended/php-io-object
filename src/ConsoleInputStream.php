<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-io-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Io;

/**
 * ConsoleInputStream class file.
 *
 * This class gets its data from the user's console.
 *
 * @author Anastaszor
 */
class ConsoleInputStream implements InputStreamInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\InputStreamInterface::read()
	 */
	public function read(int $numberOfBytes) : ?string
	{
		if(0 >= $numberOfBytes)
		{
			return '';
		}
		
		$ret = \fread(\STDIN, $numberOfBytes);
		if(false === $ret)
		{
			return null;
		}
		
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\InputStreamInterface::readline()
	 */
	public function readline() : ?string
	{
		return (string) \readline();
	}
	
}
