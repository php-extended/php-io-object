<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-io-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Io;

use Iterator;

/**
 * ConsoleOutputStream class file.
 *
 * This class outputs the current data to stdout.
 *
 * @author Anastaszor
 */
class ConsoleOutputStream implements OutputStreamInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::write()
	 */
	public function write(string $bytes) : int
	{
		\fwrite(\STDOUT, $bytes);
		
		return (int) \mb_strlen($bytes, '8bit');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeLn()
	 */
	public function writeLn(string $bytes) : int
	{
		return $this->write($bytes."\n");
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeArray()
	 */
	public function writeArray(array $bytes) : int
	{
		$len = 0;
		$key = 0;
		
		foreach($bytes as $byte)
		{
			if(0 < $key)
			{
				$byte = "\n".$byte;
			}
			$key++;
			$len += $this->write($byte);
		}
		
		return $len;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeArrayLn()
	 */
	public function writeArrayLn(array $bytes) : int
	{
		$len = 0;
		
		foreach($bytes as $byte)
		{
			$len += $this->writeLn($byte);
		}
		
		return $len;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeIterator()
	 */
	public function writeIterator(Iterator $iterator) : int
	{
		$len = 0;
		$key = 0;
		
		foreach($iterator as $byte)
		{
			if(0 < $key)
			{
				$byte = "\n".$byte;
			}
			$key++;
			$len += $this->write($byte);
		}
		
		return $len;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\OutputStreamInterface::writeIteratorLn()
	 */
	public function writeIteratorLn(Iterator $iterator) : int
	{
		$len = 0;
		
		foreach($iterator as $byte)
		{
			$len += $this->writeLn($byte);
		}
		
		return $len;
	}
	
}
