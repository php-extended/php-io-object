<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-io-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Io;

/**
 * NullInputStream class file.
 *
 * This class is provided as a placeholder when an inputstream is needed.
 * This class provides no data, and behaves like all the data source are
 * exhausted.
 *
 * @author Anastaszor
 */
class NullInputStream implements InputStreamInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\InputStreamInterface::read()
	 */
	public function read(int $numberOfBytes) : ?string
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Io\InputStreamInterface::readline()
	 */
	public function readline() : ?string
	{
		return null;
	}
	
}
